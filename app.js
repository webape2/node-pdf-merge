const http = require('http');
const fs = require('fs'), path = require("path");
const hostname = '127.0.0.1';
const port = 3000;
const PDFDocument = require('pdf-lib').PDFDocument

  allDirs = [];
const server = http.createServer((req, res) => {
  res.statusCode = 200;
  res.setHeader('Content-Type', 'text/html');
  res.write("<p>First test</p>");


  var curpath = './';

  fs.readdir(curpath, function (err, folders) {
  	
  	folders.map(function (folder) {
   
    	filePath = curpath+folder;
    		isDirectoryStats = fs.lstatSync(filePath); 
	    		//console.log('filepath', folder);
			if(isDirectoryStats.isDirectory()) {
					allDirs.push(folder);
					//console.log(allDirs);
					getPDFsfromFolder(folder, res);

			} else {
				//console.log('This is a file', folder);
			}
    });

  });//Readir end
  // res.end('Finished');
});


function getPDFsfromFolder(currentpath, res) {
  res.write("<p>PDF from folder - "+currentpath+"</p>");
	var bufferedPDFS = [];
	 fs.readdir(currentpath, function (err, singleFolder) {
	 	singleFolder.map(function (pdffile) {
	 			if(pdffile != '.DS_Store'){
	 				bufferedPDFS.push(pdffile);
	 			}
	 		})
	 		if(bufferedPDFS.length > 0 && bufferedPDFS.length == 2){
	 			combinepdfs(bufferedPDFS, currentpath, res);
	 		} else {
	 			  res.write("<p>Skipped</p>");
	 		}
	 })


	 // return 'Something';
}

async function combinepdfs(pdfs, currentpath, res) { //Combine the two pdfs into one
   	//console.log('pdfs',pdfs[0], currentpath);
   	//Read pdf1 top file buffer


   	filename = pdfs[0].slice(0, -4); 
   	filename = filename+'_com.pdf';

   	if (!fs.existsSync('./'+currentpath+'/'+filename)) {

			   	var pdf1 = fs.readFileSync('./'+currentpath+'/'+pdfs[0]); 
			   	var pdf2 = fs.readFileSync('./'+currentpath+'/'+pdfs[1]); 

			   	const firstDonorPdfDoc = await PDFDocument.load(pdf1)
				const secondDonorPdfDoc =  await PDFDocument.load(pdf2)

				const megingPDFs = await PDFDocument.create();

				 const firstPages = await megingPDFs.copyPages(firstDonorPdfDoc, firstDonorPdfDoc.getPageIndices());
				  firstPages.forEach((page) => {
			         megingPDFs.addPage(page); 
			   	 }); 

				   const secondPages = await megingPDFs.copyPages(firstDonorPdfDoc, firstDonorPdfDoc.getPageIndices());
				  secondPages.forEach((page) => {
			         megingPDFs.addPage(page); 
			   	 }); 

				  const buf = await megingPDFs.save();

				  let path = './'+currentpath+'/'+filename; 
					fs.open(path, 'w', function (err, fd) {
					    fs.write(fd, buf, 0, buf.length, null, function (err) {
					        fs.close(fd, function () {
					            console.log('wrote the file successfully');
					        }); 
					    }); 
					}); 
					if(fs.existsSync('./'+currentpath+'/'+pdfs[0])) {
					  fs.unlinkSync('./'+currentpath+'/'+pdfs[0]);
					  fs.unlinkSync('./'+currentpath+'/'+pdfs[1]);
					 }
					}
}

server.listen(port, hostname, () => {
  console.log(`Server running at http://${hostname}:${port}/`);
});
